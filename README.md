# Division Política

Divisiones políticas en CSV mantenidas por la comunidad

## Motivación y Objetivo

Las divisiones políticas producidas por entidades oficiales 
(como el DANE e IGAC en Colombia que producen oficialmente DIVIPOLA y 
mapas veredales entre otros) suelen tener fallas o en ocasiones
sus sitios oficiales de distribución de los datos no operan o cambian 
esporádicamente o no mantienen versiones históricas o no cuentan con un 
sistema de control de cambios ni facilitan contribuciones.

En este repositorio queremos mantener copia de esas fuentes oficiales,
sus conversiones a CSV (que es formato plano, abierto y estándar) y 
correcciones por parte de la comunidad.  De esta forma quien lo 
requiera pueda usarlos en cualquier proyecto de software o análisis de datos.

Te invitamos a contribuir con copias de archivos oficiales de código abierto
y con correcciones de dominio público (sustentadas por ejemplo en 
comunicaciones públicas a las entidades oficiales que producen la información 
y sus respuestas).


## Organización y Contribuciones

Hay una carpeta por país, por cada país hay una carpeta por año con
archivos "oficiales" originales preferiblemente de dominio público o
de código abierto.

En el archivo `README.md` de cada año se enumeran los archivos originales
que estén en la carpeta junto con el sitio de descarga, la fecha de descarga
y el licenciamiento de código abierto con el que lo produjo la entidad 
(preferiblemente dominio público).

Por cada archivo oficial puede haber un archivo CSV que debe ser fiel al 
original pero:
* Su nombre es de la forma `tipo_siglaentidadfuente_año.csv`
* Emplea encabezados uniformes para el país. En el caso de Colombia los
  encabezados uniformes son:
  * `departamento`: Nombre del departamento
  * `cod_departamento`: Código del departamento
  * `municipio`: Municipio
  * `cod_municipio`: Código del municipio
  * `centropoblado`: Centro poblado
  * `cod_centropoblado`: Código del centro poblado
  * `tipo_centropoblado`: Tipo del centro poblado
  * `vereda`: Nombre de vereda
  * `cod_vereda`: Código de la vereda
* Si el archivo oficial no es una hoja de cálculo en el archivo `README.md`
  describir como se generó el CSV a partir del archivo oficial.

Por cada archivo CSV de un archivo oficial, puede haber un archivo CSV
con la misma información pero con correcciones de dominio público de la 
comunidad. Debe cumplir: 
* Tener toda la información del original que no se considere errada
* El nombre del archivo debe ser de la forma 
  `tipo_siglaentidadfuente_año_corregido.csv` 
* Emplear las cabeceras uniformes
* Los cambios deben estar sustentados en:
   * Por ejemplo comunicaciones a entidades oficiales, sus respuestas, programas 
     e instrucciones que queden en la carpeta `correcciones` del mismo año
   * Historial de Solicitudes de Mezcla (Merge Request), ver
     <https://gitlab.com/pasosdeJesus/division-politica/-/merge_requests>
   * Historial de Incidentes (Issues), ver
     <https://gitlab.com/pasosdeJesus/division-politica/-/issues>
   * Historial de Contribuciones (commits), ver
     <https://gitlab.com/pasosdeJesus/division-politica/-/commits/main>


## Contribuidores

* Vladimir Támara Patiño del proyecto Pasos de Jesús. vtamara@pasosdeJesus.org
* _Contribuye y agrega tu nombre e institución_

Como puede verse en 
<https://gitlab.com/pasosdeJesus/division-politica/-/graphs/main>
