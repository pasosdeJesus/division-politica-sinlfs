* Nos falta CRVeredas_2017.rar descargado el 18.Nov.2019 desde 
  http://geoportal.dane.gov.co/servicios/descarga-y-metadatos/descarga-nivel-de-referencia-de-veredas/ 
  eligiendo 2017 y en el enlace que decía SHP.  Fue publicado con licencia 
  Creative Commons Atribución 4.0 Internacional según
  <https://web.archive.org/web/20191119221542/https://geoportal.dane.gov.co/acerca-del-geoportal/licencia-y-condiciones-de-uso/>
* Veredas_DANE_2017.csv se extrajo del anterior así: se descomprimió, se extrajo 
  CRVeredas_2017.dbf, y con LibreOffice Calc se guardo como CSV cambiando 
  nombres de encabezados a los uniformes
