#!/usr/bin/env ruby
# encoding: utf-8
#
# Dominio público de acuerdo a la legislazción colombiana.
# vtamara@pasosdeJesus.org 2021
#
# Analiza archivo con veredas respecto a un DIVIPOLA y reporta errores
# generando en sálida estándar un CSV y por error estándar un resumen
#
# La forma de uso típica es
#
# analiza-archivo-veredas.rb DIVIPOLA_DANE_2020_08.csv Veredas_DANE_2020.csv >  correcciones/problemas_Veredas_DANE_2020.csv 2> /tmp/resumen.txt

require_relative '../../utilidades/bitprobcsv.rb'
require 'csv'

if ARGV.length < 2 || ARGV.length > 3
  STDERR.puts "Primer argumento debe ser DIVIPOLA con campos "\
    "departamento,municipio,centropoblado,cod_centropoblado"
  STDERR.puts "Segundo debe ser archivo de veredas con campos "\
    "departamento,municipio,cod_municipio,vereda,cod_vereda"
  STDERR.puts "Tercer argumento opcional es nombre de archivo "\
    "donde escribir CSV con correcciones"
  exit 1
end

def normaliza(texto)
  if texto.nil? || texto == '' || texto == 'null'
    return nil
  end
  r = texto.downcase.gsub('á', 'a')
  r.gsub!('Á', 'a')
  r.gsub!('é', 'e')
  r.gsub!('É', 'e')
  r.gsub!('í', 'i')
  r.gsub!('Í', 'i')
  r.gsub!('ó', 'o')
  r.gsub!('Ó', 'o')
  r.gsub!('ú', 'u')
  r.gsub!('Ú', 'u')
  r.gsub!('ü', 'u')
  r.gsub!('Ü', 'u')
  r
end

def a_mayusculas(texto)
  if texto.nil? || texto == '' || texto == 'null'
    return ''
  end
  r = texto.upcase
  r.gsub!('á', 'Á')
  r.gsub!('é', 'É')
  r.gsub!('í', 'Í')
  r.gsub!('ó', 'Ó')
  r.gsub!('ú', 'Ú')
  r.gsub!('ü', 'Ü')
  r
end



ndiv = ARGV[0]
STDERR.puts "Leyendo DIVIPOLA #{ndiv}"
divcsv = CSV.read(ndiv, headers: true)
STDERR.puts "Se leyeron #{divcsv.count} registros de #{ndiv}"

encdiv = divcsv[0].headers
if !encdiv.include?('departamento') ||
    !encdiv.include?('cod_municipio') || !encdiv.include?('municipio') ||
    !encdiv.include?('cod_centropoblado') || !encdiv.include?('centropoblado')

  STDERR.puts "DIVIPOLA no tiene encabezados necesarios: departamento, "\
    "cod_municipio, municipio, cod_centropoblado, centropoblado."
end

# DIVIPOLA estilo árbol indexado por nombres de deptos, nombres 
# de municipios y nombres de centros poblados
adiv = {} 

#Municipios indexados por código
divmun = {}

# Centros poblados indexados por código
divcp = {}

numr = 0
divcsv.each do |c|
  numr += 1
  if !c['departamento'] || !c['municipio'] || !c['centropoblado'] || 
      !c['cod_municipio'] || !c['cod_centropoblado']
    STDERR.puts "* #{ndiv}:#{numr}: Registro sin departamento o municipio o "\
      "centropoblado"
  else
    cdep = a_mayusculas(c['departamento'])
    cmun = a_mayusculas(c['municipio'])
    ccp = a_mayusculas(c['centropoblado'])
    codmun = c['cod_municipio']
    codcp = c['cod_centropoblado']
    if !adiv[cdep]
      adiv[cdep] = {}
      adiv[cdep][cmun] = {}
      divmun[codmun] = cmun
    elsif !adiv[cdep][cmun]
      adiv[cdep][cmun] = {}
      divmun[codmun] = cmun
    end
    adiv[cdep][cmun][ccp] = codcp
    divcp[codcp] = ccp
  end
end
STDERR.puts "Creados índices con DIVIPOLA"

nver = ARGV[1]
STDERR.puts "Leyendo archivo con veredas #{nver}"
vercsv = CSV.read(nver, headers: true)
STDERR.puts "Se leyeron #{vercsv.count} registros de #{nver}"

encver = vercsv[0].headers
if !encver.include?('departamento') ||
    !encver.include?('cod_municipio') || !encver.include?('municipio') ||
    !encver.include?('cod_vereda') || !encver.include?('vereda')

  STDERR.puts "Archivo con veredas no tiene encabezados necesarios: "\
  " departamento, cod_municipio, municipio, cod_vereda, vereda."
end


bitprob = Bitprobcsv.new

# Árbol con veredas indexado por departamento, municipio y vereda con valor 
# código de vereda
aver = {} 
# Indice de veredas por código, el valor es la fila en la que está
iver = {}

inccod = 0
vrep = 0
vsin = 0
vsd = 0
vcrep = 0

salida = [] # Registros por escribir en archivo de salida

errnom = 0
maldep = 0
malmun = 0
malcodmun = 0
malnommun = 0
codrepcp = 0
vrepcp = 0
nreg = 0
vercsv.each do |r|
  nreg += 1
  aceptable = true # Si es true puede escribirse en archivo con correcciones
  if !r['departamento'] || !r['municipio'] ||
      !r['cod_municipio'] || !r['cod_vereda'] || !r['vereda']
    STDERR.puts "* #{ndiv}:#{nreg}: Registro sin departamento o municipio "\
      "o cenuropoblado o cod_municipio o cod_vereda o vereda"
  else
    cdep = r['departamento']
    cmun = r['municipio']
    cver = r['vereda']
    codmun = r['cod_municipio']
    codver = r['cod_vereda']
    coddep = codmun.length > 2 ? codmun[0..1] : '00'
    if !adiv[cdep]
      bitprob.reg_prob(nreg, codmun, cdep, cmun, codver, cver,
            "No se encontró departamento en el DIVIPOLA")
      maldep += 1
      aceptable = false
    elsif !adiv[cdep][cmun]
      bitprob.reg_prob(nreg, codmun, cdep, cmun, codver, cver,
            "No se encontró municipio en el DIVIPOLA")
      malmun += 1
      aceptable = false
    elsif adiv[cdep][cmun][cver]
      bitprob.reg_prob(nreg, codmun, cdep, cmun, codver, cver,
        "Centro poblado #{adiv[cdep][cmun][cver]} en DIVIPOLA "\
        "tiene el mismo nombre de la vereda")
      vrepcp += 1
      aceptable = false
    end
    if !divmun[codmun] 
      bitprob.reg_prob(nreg, codmun, cdep, cmun, codver, cver,
            "No se encontró código de municipio en el DIVIPOLA")
      malcodmun += 1
      aceptable = false
    end
    if divmun[codmun] != cmun
      bitprob.reg_prob(nreg, codmun, cdep, cmun, codver, cver,
        "Nombre de municipio no corresponde a '#{divmun[codmun]}' que según "\
        "el DIVIPOLA es el del municipio con código #{codmun} ")
      malnommun += 1
      aceptable = false
    end
    if divcp[codver]
      bitprob.reg_prob(nreg, codmun, cdep, cmun, codver, cver,
        "En DIVIPOLA hay un centro poblado (#{divcp[codver]}) con el mismo "\
        "código de esta vereda")
      codrepcp += 1
    end

    # Buscar errores en nombre de vereda
    if (cver =~ /[^ ]\(/)
      bitprob.reg_prob(
        nreg, codmun, cdep, cmun, codver, cver,
        "En nombre de vereda falta espacio antes de abrir paréntesis")
      errnom += 1
    end
    if (cver =~ /\)[^ ]/)
      bitprob.reg_prob(
        nreg, codmun, cdep, cmun, codver, cver,
        "En nombre de vereda falta espacio después de cerrar paréntesis")
      errnom += 1
    end
    if (cver =~ /-[^ ]/)
      bitprob.reg_prob(
        nreg, codmun, cdep, cmun, codver, cver,
        "En nombre de vereda falta espacio después de guión")
      errnom += 1
    end
    if (cver =~ /[^ ]-/)
      bitprob.reg_prob(
        nreg, codmun, cdep, cmun, codver, cver,
        "En nombre de vereda falta espacio antes de guión")
      errnom += 1
    end
    if (cver =~ /[^-A-Za-z0-9 \(\)ÁÉÍÓÚÜáéíóúñÑ]/)
      bitprob.reg_prob(
        nreg, codmun, cdep, cmun, codver, cver,
        "Nombre de vereda tiene caracteres que no son del español")
      errnom += 1
    end
    if (cver =~ /  /)
      bitprob.reg_prob(
        nreg, codmun, cdep, cmun, codver, cver,
        "Nombre de vereda tiene doble espacio en blanco")
      errnom += 1
    end
    if (cver =~ /[^A-Z]EL [A-Z]*S[^A-Z]/)
      bitprob.reg_prob(
        nreg, codmun, cdep, cmun, codver, cver,
        "En nombre de vereda no coincide artículo singular con sustantivo plural")
      errnom += 1
    end
    if (cver =~ /[^A-Z]LA [A-Z]*S[^A-Z]/)
      bitprob.reg_prob(
        nreg, codmun, cdep, cmun, codver, cver,
        "En nombre de vereda no coincide artículo singular con sustantivo plural")
      errnom += 1
    end
    if (cver =~ /[^A-Z]LAS [^ ,]*[^S][ ,]/)
      bitprob.reg_prob(
        nreg, codmun, cdep, cmun, codver, cver,
        "En nombre de vereda no coincide artículo plural con sustantivo singular")
      errnom += 1
    end
    if (cver =~ /[^A-Z]LOS [^ ,]*[^S][ ,]/)
      bitprob.reg_prob(
        nreg, codmun, cdep, cmun, codver, cver,
        "En nombre de vereda no coincide artículo plural con sustantivo singular")
      errnom += 1
    end
    if (cver =~ /\.$/)
      bitprob.reg_prob(
        nreg, codmun, cdep, cmun, codver, cver,
        "En nombre de vereda sobra punto final")
      errnom += 1
    end

    if !aver[cdep]
      aver[cdep] = {}
      aver[cdep][cmun] = {}
    elsif !aver[cdep][cmun]
      aver[cdep][cmun] = {}
    end
    if cver == 'SIN INFORMACION'
      bitprob.reg_prob(nreg, codmun, cdep, cmun, codver, cver,
            "Nombre de vereda SIN INFORMACION")
      vsin += 1
      aceptable = false
    elsif cver == 'SD'
      bitprob.reg_prob(nreg, codmun, cdep, cmun, codver, cver,
            "Nombre de vereda SD")
      vsd+= 1
      aceptable = false

    elsif aver[cdep][cmun][cver]
      vrep += 1
      bitprob.reg_prob(nreg, codmun, cdep, cmun, codver, cver,
        "Nombre de vereda repetido en el mismo municipio "\
        "con código(s): #{aver[cdep][cmun][cver]} ")
      aver[cdep][cmun][cver] += " #{codver}"
      aceptable = false
    else
      aver[cdep][cmun][cver] = codver
    end
    if codver[0..(codmun.length-1)] != codmun
      bitprob.reg_prob(nreg, codmun, cdep, cmun, codver, cver,
            "Código de vereda no concuerda con código de municipio")
      inccod += 1
      aceptable = false
    end
    if iver[codver]
      bitprob.reg_prob(nreg, codmun, cdep, cmun, codver, cver,
        "Código de vereda #{codver} de la fila #{nreg} repetido en "\
        "fila(s) #{iver[codver]}")
      iver[codver] += " #{nreg.to_s}"
      vcrep += 1
      aceptable = false
    else
      iver[codver] = nreg.to_s
    end

    if aceptable
      salida << [codmun, codver, cdep, cmun, cver, coddep, nreg]
    end
  end
end

STDERR.puts "Veredas cuyo nombre de departamento no está en DIVIPOLA: #{maldep}"
STDERR.puts "Veredas cuyo nombre de municipio no está en DIVIPOLA: #{malmun}"
STDERR.puts "Veredas cuyo código de municipio no está en DIVIPOLA: #{malcodmun}"
STDERR.puts "Veredas cuyo nombre de municipio no corresponde al del "\
  "mismo código en DIVIPOLA: #{malnommun}"
STDERR.puts "Veredas con nombre igual al de un centro poblado "\
  "del mismo municipio: #{vrepcp}"
STDERR.puts "Veredas que tienen el mismo código de un "\
  "centro poblado: #{codrepcp}"
STDERR.puts "Veredas con problema ortográfico en nombre: #{errnom}"


STDERR.puts "Veredas con nombre SIN INFORMACION: #{vsin}"
STDERR.puts "Veredas con nombre SD: #{vsd}"
STDERR.puts "Veredas repetidas en el mismo municipio: #{vrep}"
STDERR.puts "Veredas con código repetido: #{vcrep}"
STDERR.puts "Veredas cuyo código no concuerda con el del municipio: #{inccod}"
STDERR.puts "Veredas analizadas: #{nreg}"


dsinv = 0
dconv = 0
msinv = 0
STDERR.puts "Verificando que haya veredas para todos los municipios del DIVIPOLA"
mcub = 0
adiv.each do |ad, rad|
  if !aver[ad] 
    STDERR.puts "No hay veredas para el departamento #{ad}"
    dsinv += 1
  else
    dconv += 1
    rad.each do |am, ram|
      if !aver[ad][am]
        STDERR.puts "No hay veredas para el municipio #{am} / #{ad}"
        msinv += 1
      else
        mcub += 1
      end
    end
  end
end
STDERR.puts "Departamentos con alguna vereda: #{dconv}"
STDERR.puts "Departamentos sin veredas: #{dsinv}"
STDERR.puts "Municipios con alguna vereda: #{mcub}"
STDERR.puts "Municipios sin veredas: #{msinv}"
STDERR.puts "Registros con problemas: #{bitprob.numreg}"
STDERR.puts "Fila más problemática: #{bitprob.fila_mas}"

bitprob.gen_csv_prob

if ARGV.length == 3
  nsal = ARGV[2]
  STDERR.puts "Escribiendo en #{nsal}"

  numsal = 0

  CSV.open(nsal, "wb") do |csv|
    csv << ['cod_municipio','cod_vereda','departamento','municipio','vereda','cod_departamento','filaorig']
    salida.each do |f|
      csv << f
      numsal += 1
    end
  end
  puts "Registros escritos: #{numsal}"
end
